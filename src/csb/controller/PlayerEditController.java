package csb.controller;

import static csb.CSB_PropertyType.REMOVE_ITEM_MESSAGE;
import csb.data.Assignment;
import csb.data.Course;
import csb.data.CourseDataManager;
import csb.data.Lecture;
import csb.data.Player;
import csb.data.PlayerItem;
import csb.data.ScheduleItem;
import csb.gui.AssignmentDialog;
import csb.gui.CSB_GUI;
import csb.gui.EditPlayerDialog;
import csb.gui.LectureDialog;
import csb.gui.MessageDialog;
import csb.gui.PlayerDialog;
import csb.gui.ScheduleItemDialog;
import csb.gui.YesNoCancelDialog;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author McKillaGorilla
 */
public class PlayerEditController {
    ScheduleItemDialog sid;
    LectureDialog ld;
    EditPlayerDialog ed;
    PlayerDialog pd;
    AssignmentDialog ad;
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    
    public PlayerEditController(Stage initPrimaryStage, MessageDialog initMessageDialog, YesNoCancelDialog initYesNoCancelDialog) {
        //sid = new ScheduleItemDialog(initPrimaryStage,  initMessageDialog);
        ed = new EditPlayerDialog(initPrimaryStage);
        pd = new PlayerDialog(initPrimaryStage);
        //ad = new AssignmentDialog(initPrimaryStage, initMessageDialog);
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
    }

    // THESE ARE FOR SCHEDULE ITEMS
    public void handleEditPlayerRequest(CSB_GUI gui, PlayerItem playerToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        ed.showEditPlayerDialog(playerToEdit);
        
        // DID THE USER CONFIRM?
        if (pd.wasCompleteSelected()) {
            // UPDATE THE SCHEDULE ITEM
            PlayerItem l = pd.getPlayerItem();
            playerToEdit.setFirst(l.getFirst());
            playerToEdit.setProTeam(l.getProTeam());
            
            // THE COURSE IS NOW DIRTY, MEANING IT'S BEEN 
            // CHANGED SINCE IT WAS LAST SAVED, SO MAKE SURE
            // THE SAVE BUTTON IS ENABLED
            gui.getFileController().markAsEdited(gui);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }              
    }
   
    public void handleAddPlayerRequest(CSB_GUI gui, TableColumn first, TableColumn last, TableColumn team,TableColumn position ) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        pd.showAddPlayerDialog();
        
        // DID THE USER CONFIRM?
        if (pd.wasCompleteSelected()) {
            // GET THE SCHEDULE ITEM
            PlayerItem l = pd.getPlayerItem();
            
            // AND ADD IT AS A ROW TO THE TABLE
            PlayerItem newPlayer = new PlayerItem();
            newPlayer.setFirst(l.getFirst());
            newPlayer.setLast(l.getLast());
            newPlayer.setProTeam(l.getProTeam());
            newPlayer.setPosition(l.getPosition());
            
           
            
            //course.addPlayer(newPlayer);
            
            // THE COURSE IS NOW DIRTY, MEANING IT'S BEEN 
            // CHANGED SINCE IT WAS LAST SAVED, SO MAKE SURE
            // THE SAVE BUTTON IS ENABLED
            gui.getFileController().markAsEdited(gui);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
        
    }
    
    
  
    
    public void handleRemovePlayerRequest(CSB_GUI gui, TableView<PlayerItem> playersTable) {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();

        // IF THE USER SAID YES, THEN REMOVE IT
        if (selection.equals(YesNoCancelDialog.YES)) { 
            gui.getDataManager().getCourse().removeScheduleItem(playersTable);
            
                    
//gui.getDataManager().getCourse().removeAssignment(playerToRemove);
            
            // THE COURSE IS NOW DIRTY, MEANING IT'S BEEN 
            // CHANGED SINCE IT WAS LAST SAVED, SO MAKE SURE
            // THE SAVE BUTTON IS ENABLED
            gui.getFileController().markAsEdited(gui);
        }
    }
}