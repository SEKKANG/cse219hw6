/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csb.data;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.CheckBox;

/**
 *
 * @author McKillaGorilla
 */
public class Player {
    final StringProperty topic;
    
    final StringProperty lastName;
    final StringProperty sessions;
    final StringProperty position;
    
    public static final String DEFAULT_TOPIC = "";
    public static final String DEFAULT_LAST_NAME = "";
    
    public static final String DEFAULT_SESSIONS = "";    
    
    
    public Player() {
        topic = new SimpleStringProperty(DEFAULT_TOPIC);
        lastName = new SimpleStringProperty(DEFAULT_LAST_NAME);
        sessions = new SimpleStringProperty(DEFAULT_SESSIONS);
        
        position = new SimpleStringProperty();
    }
    
    public void reset() {
        setTopic(DEFAULT_TOPIC);
        setSessions(DEFAULT_SESSIONS);
    }
    
    public String getTopic() {
        return topic.get();
    }
    
    public void setTopic(String initTopic) {
        topic.set(initTopic);
    }
    
    public StringProperty topicProperty() {
        return topic;
    }
    public String getLastName() {
        return lastName.get();
    }
    
    public void setLastName(String initTopic) {
        lastName.set(initTopic);
    }
    
    public StringProperty lastNameProperty() {
        return lastName;
    }
    
    public String getSessions() {
        return sessions.get();
    }
    
    public void setSessions(String initSessions) {
        sessions.set(initSessions);
    }
    
    public StringProperty sessionsProperty() {
        return sessions;
    }
     public String getPosition() {
        return position.get();
    }
    
    public void setPosition(String initSessions) {
        position.set(initSessions);
    }
    
    public StringProperty positionProperty() {
        return position;
    }
    
}
