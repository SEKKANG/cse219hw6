/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csb.data;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author McKillaGorilla
 */
public class Lecture {
    final StringProperty topic;
    final StringProperty sessions;
    public static final String DEFAULT_TOPIC = "";
    public static final String DEFAULT_SESSIONS = "";    
    
    public Lecture() {
        topic = new SimpleStringProperty(DEFAULT_TOPIC);
        sessions = new SimpleStringProperty(DEFAULT_SESSIONS);
    }
    
    public void reset() {
        setTopic(DEFAULT_TOPIC);
        setSessions(DEFAULT_SESSIONS);
    }
    
    public String getTopic() {
        return topic.get();
    }
    
    public void setTopic(String initTopic) {
        topic.set(initTopic);
    }
    
    public StringProperty topicProperty() {
        return topic;
    }
    
    public String getSessions() {
        return sessions.get();
    }
    
    public void setSessions(String initSessions) {
        sessions.set(initSessions);
    }
    
    public StringProperty sessionsProperty() {
        return sessions;
    }
}
