package csb.gui;

import csb.data.Lecture;
import csb.data.Player;
import csb.data.PlayerItem;
import static csb.gui.CSB_GUI.CLASS_HEADING_LABEL;
import static csb.gui.CSB_GUI.CLASS_PROMPT_LABEL;
import static csb.gui.CSB_GUI.PRIMARY_STYLE_SHEET;
import static csb.gui.LectureDialog.ADD_LECTURE_TITLE;
import java.time.LocalDate;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author McKillaGorilla
 */
public class EditPlayerDialog extends Stage {
    // THIS IS THE OBJECT DATA BEHIND THIS UI
    PlayerItem playerItem;
    
    
    // GUI CONTROLS FOR OUR DIALOG
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    
    Label fantasyTeamLabel;
    ComboBox fantasyTeamComboBox;
    
    Label positionLabel;
    ComboBox positionComboBox;
    
    Label contractLabel;
    ComboBox contractComboBox;
    
    Label salaryLabel;
    TextField salaryTextField;
    
    
    ComboBox az;
    ComboBox atl;
    ComboBox chc;
    ComboBox cin;
    ComboBox col;
    ComboBox lad;
    ComboBox mia;
    ComboBox mil;
    ComboBox nym;
    ComboBox phi;
    ComboBox pit;
    ComboBox sd;
    ComboBox sf;
    ComboBox stl;
    ComboBox tor;
    ComboBox wsh;
  
            
    Button completeButton;
    Button cancelButton;
    
    HBox positionCheckBox;
    
    CheckBox c;
    CheckBox firstB;
    CheckBox thirdB;
    CheckBox secondB;
    CheckBox ss;
    CheckBox of;
    CheckBox p;
    
    
    // THIS IS FOR KEEPING TRACK OF WHICH BUTTON THE USER PRESSED
    String selection;
    
    // CONSTANTS FOR OUR UI
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String FANTASY_TEAM_PROMPT = "Fantasy Team: ";
    
    public static final String POSITION_PROMPT = "Position: ";
    public static final String CONTRACT_PROMPT = "Contract: ";
    public static final String SALARY_PROMPT = "Salary($): ";
    
    public static final String LECTURE_HEADING = "Player Details";
    public static final String ADD_LECTURE_TITLE = "Add New Player";
    public static final String EDIT_LECTURE_TITLE = "Edit Player";
    /**
     * Initializes this dialog so that it can be used for either adding
     * new schedule items or editing existing ones.
     * 
     * @param primaryStage The owner of this modal dialog.
     */
    public EditPlayerDialog(Stage primaryStage) {
        // FIRST MAKE OUR LECTURE AND INITIALIZE
        // IT WITH DEFAULT VALUES
        playerItem = new PlayerItem();
        
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        // PUT THE HEADING IN THE GRID, NOTE THAT THE TEXT WILL DEPEND
        // ON WHETHER WE'RE ADDING OR EDITING
        headingLabel = new Label(LECTURE_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
    
        // NOW THE TOPIC 
        fantasyTeamLabel = new Label(FANTASY_TEAM_PROMPT);
        fantasyTeamLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        fantasyTeamComboBox = new ComboBox();
        
        positionLabel = new Label(POSITION_PROMPT);
        positionLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        positionComboBox = new ComboBox();
        
        // AND THE NUMBER OF SESSIONS
        contractLabel = new Label(CONTRACT_PROMPT);
        contractLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        contractComboBox = new ComboBox();
        
        salaryLabel = new Label(SALARY_PROMPT);
        salaryLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        salaryTextField = new TextField();
        
        fantasyTeamComboBox.getItems().addAll("Free Agent","     ");
        contractComboBox.getItems().addAll("S2","S1"," ");
        positionComboBox.getItems().addAll("1B","2B","3B","CI","C","OP","P","MI");
        positionComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                String numSessions = newValue.toString();
                playerItem.setProTeam(numSessions);
            }
            
        });
        positionCheckBox = new HBox();
        c = new CheckBox("C  ");
        firstB= new CheckBox("1B  ");
        thirdB= new CheckBox("3B  ");
        secondB= new CheckBox("2B  ");
        ss= new CheckBox("SS  ");
        of= new CheckBox("OF  ");
        p = new CheckBox("P  ");
        
        positionCheckBox.getChildren().add(c);
        positionCheckBox.getChildren().add(firstB);
        positionCheckBox.getChildren().add(thirdB);
        positionCheckBox.getChildren().add(secondB);
        positionCheckBox.getChildren().add(ss);
        positionCheckBox.getChildren().add(of);
        positionCheckBox.getChildren().add(p);
        
        // AND FINALLY, THE BUTTONS
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            EditPlayerDialog.this.selection = sourceButton.getText();
            EditPlayerDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);

        // NOW LET'S ARRANGE THEM ALL AT ONCE
        gridPane.add(headingLabel,      0, 0, 2, 1);
        gridPane.add(fantasyTeamLabel,        0, 1, 1, 1);
        gridPane.add(fantasyTeamComboBox,    1, 1, 1, 1);
        
        gridPane.add(positionLabel,        0, 2, 1, 1);
        gridPane.add(positionComboBox,    1, 2, 1, 1);
        gridPane.add(contractLabel,     0, 3, 1, 1);
        gridPane.add(contractComboBox,  1, 3, 1, 1);
        gridPane.add(salaryLabel,  0, 4, 1, 1);
        gridPane.add(salaryTextField,  1, 4, 1, 1);
        gridPane.add(completeButton,    0, 5, 1, 1);
        gridPane.add(cancelButton,      1, 5, 1, 1);

        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }

    
    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }
    
    public PlayerItem getPlayerItem() { 
        return playerItem;
    }
    
    /**
     * This method loads a custom message into the label and
     * then pops open the dialog.
     * 
     * @param message Message to appear inside the dialog.
     */
   // Player add show dialog
    public PlayerItem showAddPlayerDialog() {
        // SET THE DIALOG TITLE
        setTitle(ADD_LECTURE_TITLE);
        
        // RESET THE SCHEDULE ITEM OBJECT WITH DEFAULT VALUES
        playerItem = new PlayerItem();
        
        // LOAD THE UI STUFF
        //topicTextField.setText(playerItem.getFirst());
        //lastNameTextField.setText(playerItem.getLast());
        //sessionsComboBox.getSelectionModel().select(0);
        
        // AND OPEN IT UP
        this.showAndWait();
        
        return playerItem;
    }
    
    public void loadGUIData() {
        // LOAD THE UI STUFF
        //topicTextField.setText(playerItem.getFirst());
        
        //String sessionsIndex = lecture.getSessions()-1;
        //sessionsComboBox.getSelectionModel().select(sessionsIndex);
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    
    public void showEditPlayerDialog(PlayerItem playerToEdit) {
        // SET THE DIALOG TITLE
        setTitle(EDIT_LECTURE_TITLE);
        
        // LOAD THE LECTURE INTO OUR LOCAL OBJECT
        playerItem.setFirst(playerToEdit.getFirst());
        playerItem.setProTeam(playerToEdit.getProTeam());
        
        // AND THEN INTO OUR GUI
        loadGUIData();
               
        // AND OPEN IT UP
        this.showAndWait();
    }
}